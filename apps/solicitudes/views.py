from django.contrib.auth.models import User

from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from apps.empresas.models import Empresa, AsesorEmpresa
from .models import Solicitud, Comentario
from .serializers import SolicitudSerializer, ComentarioSerializer


class SolicitudList(APIView):
    parser_classes = (MultiPartParser, FormParser)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Para los clientes genera un listado de todas sus solicitudes

        @return JSON
        """
        solicitudes = null = None
        if request.user.persona.rol == '0':
            solicitudes = Solicitud.objects.filter(
                autor=request.user.persona.id
            )
        serializer = SolicitudSerializer(solicitudes, many=True)

        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        """
        Permite registrar una nueva solicitud a un cliente

        @return Estado 201 de confirmación
        Si se autentifica con un token no relacionado a un cliente
        obtendrá un código 401
        Si la empresa no existe retorna un 404
        """
        if request.user.persona.rol == '0':
            asunto = request.data['asunto']
            descripcion = request.data['descripcion']
            empresa = request.data['empresa_id']

            try:
                Empresa.objects.get(id=empresa)
                solicitud = Solicitud.objects.create(
                    asunto=asunto,
                    descripcion=descripcion,
                    autor_id=request.user.persona.id,
                    empresa_id=empresa
                )
                solicitud.save()

                return Response(status=status.HTTP_201_CREATED)
            except Empresa.DoesNotExist:
                return Response("Empresa no existe",status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_401_UNAUTHORIZED)

class SolicitudSinAsignarList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Listado de todas las solicitudes sin asignar

        @return JSON
                Si el usuario no es administrador retorna un estado 401
        """
        if request.user.persona.rol == '2':
            solicitudes = Solicitud.objects.filter(asesor=None)
            serializer = SolicitudSerializer(solicitudes, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class SolicitudDetail(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Solicitud.objects.get(id=pk)
        except Solicitud.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        Obtiene información de una solicitud

        @return JSON
        """
        solicitud = self.get_object(pk)
        serializer = SolicitudSerializer(solicitud)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """
        Marca como solucionado una solicitud

        @return Estados HTTP
                Estado 200 cuando se soluciona la solicitud
                Estado 401 si alguien diferente al autor o asesor intentan modificar
                Estado 404 si no existe la solicitud
        """
        solicitud = self.get_object(pk)
        if solicitud.autor_id == request.user.persona.id or solicitud.asesor_id == request.user.persona.id:
            solicitud.resuelto = True
            solicitud.save()

            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class AsesorConsultaList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Asigna una consulta a un asesor

        @return Estado 201 de creación
                Si se autentifica con un token no relacionado a un administrador
                obtendrá un código 401
                Si el usuario no tiene rol de asesor retorna un 404
                Retorna 409 si el asesor no está asociado a la empresa de la solicitud
        """
        if request.user.persona.rol == '2':
            consulta = request.data['consulta_id']
            asesor = request.data['asesor_id']

            if User.objects.get(id=asesor).persona.rol == '1':
                solicitud = Solicitud.objects.get(id=consulta)

                if AsesorEmpresa.objects.filter(asesor_id=asesor).filter(empresa_id=solicitud.empresa).exists():
                    solicitud.asesor_id = asesor
                    solicitud.save()

                    return Response(status=status.HTTP_201_CREATED)
                else:
                    return Response(status=status.HTTP_409_CONFLICT)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class AgregarComentarioList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, solicitud):
        try:
            return Solicitud.objects.get(id=solicitud)
        except Solicitud.DoesNotExist:
            raise Http404

    def post(self, request, *args, **kwargs):
        """
        Agrega un comentario en una solicitud

        @return Estado 201 de creación
                Estado 401 si el comentador no es autor ni asesor de la solicitud
                Estado 404 si no existe la solicitud
        """
        autor = request.data['autor_id']
        solicitud = request.data['solicitud_id']

        info_solicitud = self.get_object(solicitud)
        if info_solicitud.autor_id == autor or info_solicitud.asesor_id == autor:
            info = request.data['comentario']

            comentario = Comentario.objects.create(
                info=info,
                autor_id=autor,
                solicitud_id=solicitud
            )
            comentario.save()

            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ComentarioList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Comentario.objects.filter(solicitud_id=pk).order_by('timestamp')
        except Comentario.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        Obtiene el listado de comentarios de una solicitud

        @return JSON con informacioń o estados HTTP
                Retorna 404 si no existen comentarios para esa solicitud
        """
        comentario = self.get_object(pk)

        if request.user.persona.rol == '0' and comentario[0].solicitud.autor_id != request.user.persona.id:
            print("Tío, tú no eres el cliente que creó la solicitud, no seas malo")
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        serializer = ComentarioSerializer(comentario, many=True)

        return Response(serializer.data)
