from django.conf.urls import url
from django.urls import path

from apps.solicitudes import views as vista

urlpatterns = [
    path('solicitud/', vista.SolicitudList.as_view()),
    path('solicitudes/libres/', vista.SolicitudSinAsignarList.as_view()),
    path('solicitud/<int:pk>/', vista.SolicitudDetail.as_view()),
    path('solicitud/asesor/', vista.AsesorConsultaList.as_view()),
    path('comentario/', vista.AgregarComentarioList.as_view()),
    path('solicitud/<int:pk>/comentarios/', vista.ComentarioList.as_view()),
]
