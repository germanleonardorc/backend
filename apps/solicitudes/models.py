from django.db import models

from apps.usuarios.models import Persona
from apps.empresas.models import Empresa as empresa


class Solicitud(models.Model):
    def get_upload_to(self, filename):
        return "imagenes/%s" % (filename)

    asunto = models.CharField(max_length=70, null=False)
    descripcion = models.CharField(max_length=280, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    autor = models.ForeignKey(Persona, on_delete=models.CASCADE, null=False)
    empresa = models.ForeignKey(empresa, on_delete=models.CASCADE, null=False)
    asesor = models.ForeignKey(Persona, on_delete=models.SET_NULL, related_name='+', null=True)
    resuelto = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Imagen'
        verbose_name_plural = 'Imagenes'

    def __str__(self):
        return self.asunto

class Comentario(models.Model):
    info = models.CharField(max_length=140, null=False)
    autor = models.ForeignKey(Persona, on_delete=models.DO_NOTHING, null=False)
    solicitud = models.ForeignKey(Solicitud, on_delete=models.CASCADE, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
