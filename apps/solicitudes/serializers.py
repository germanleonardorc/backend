from rest_framework import serializers

from .models import Solicitud, Comentario
from apps.usuarios.serializers import PersonaSerializer
from apps.empresas.serializers import EmpresaSerializer

class SolicitudSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    autor = PersonaSerializer(read_only=True)
    empresa = EmpresaSerializer(read_only=True)
    asesor = PersonaSerializer(read_only=True)

    class Meta():
        model = Solicitud
        fields = (
            'id',
            'asunto',
            'descripcion',
            'timestamp',
            'autor',
            'resuelto',
            'empresa',
            'asesor'
        )

class ComentarioSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    autor = PersonaSerializer(read_only=True)

    class Meta():
        model = Comentario
        fields = (
            'id',
            'info',
            'autor',
            'solicitud',
            'timestamp'
        )
