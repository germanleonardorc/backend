import json

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from apps.solicitudes.models import Solicitud
from .models import Persona
from apps.solicitudes.serializers import SolicitudSerializer
from .serializers import PersonaSerializer


def Bienvenido(request):
    return render(request, 'welcome.html')

class Usuario:
    def registro(self, nombre, apellido, email, password, rol):
        """
        Centraliza el registro de usuarios para los distintos roles, más información en las vistas indicadas.
        """
        user = User.objects.create_user(email, email, password)
        user.save()

        usuario = Persona(usuario=user, nombre=nombre, apellido=apellido, rol=rol)
        usuario.save()

class SuperRegistro(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        """
        Da un espacio para el registro de usuarios con privilegios de Administrador o Asesor en el sistema.

        Debe existir una sesión de administración.

        @return Estados HTTP de información
                401 si la petición se hace desde un usuario no autorizado
                406 si el correo ya está registrado
                201 si se registra correctamente
        """
        if request.user.persona.rol == '2':
            nombre = request.data['nombre']
            apellido = request.data['apellido']
            email = request.data['email']
            password = request.data['password']
            rol = request.data['rol']

            if User.objects.filter(username=email).exists():
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                usuario = Usuario()
                usuario.registro(nombre,apellido,email,password,rol)

                return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class RegistroCliente(APIView):
    def post(self, request, format=None):
        """
        Da un espacio a los clientes para su registro en el sistema

        @return Estados HTTP de información
                un estado 406 si el correo ya está registrado
                un estado 201 si se registra correctamente
        """
        nombre = request.data['nombre']
        apellido = request.data['apellido']
        email = request.data['email']
        password = request.data['password']

        if User.objects.filter(username=email).exists():
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            usuario = Usuario()
            usuario.registro(nombre,apellido,email,password,rol=0)

            return Response(status=status.HTTP_201_CREATED)

class Login(APIView):
    def post(self, request, format=None):
        """
        Inicia sesión en el sistema

        @return JSON con id de usuario, código de permisos y token.
                Si existe algún error retornará estados del tipo 4XX
        """
        email = request.data['email']
        password = request.data['password']

        user = authenticate(username=email, password=password)

        try:
            us = User.objects.get(username=email)

            if user is not None:
                try:
                    token = Token.objects.create(user=user)
                except:
                    token = Token.objects.get(user=user)

                objeto = {
                    'id': us.pk,
                    'permisos': us.persona.rol,
                    'token': token.key
                }

                return HttpResponse(
                    json.dumps(objeto),
                    content_type="application/json"
                )

            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

class PersonaDetail(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Obtiene la información al detalle de la persona que se ha identificado

        Es útil para mostrar la informaciónd de su perfil

        @return JSON
        """

        persona = Persona.objects.filter(id=request.user.persona.id)
        serializer = PersonaSerializer(persona, many=True)

        return Response(serializer.data)

class AsesorList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Obtiene un listado de todos los asesores registrados

        @return JSON
                sólo responde a peticiones de usuarios administradores, si no lo
                es retornará un estado 401
        """

        if request.user.persona.rol == '2':
            asesores = Persona.objects.filter(rol='1')
            serializer = PersonaSerializer(asesores, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class SolicitudAsesorList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Solicitud.objects.filter(asesor=pk)
        except Solicitud.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        Listado de todas las solicitudes asignadas a un asesor

        @return JSON
                Si el usuario no es administrador retorna un estado 401
                Retorna un estado 404 si no existe la empresa
        """
        if request.user.persona.rol == '2':
            solicitudes = self.get_object(pk)
            serializer = SolicitudSerializer(solicitudes, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
