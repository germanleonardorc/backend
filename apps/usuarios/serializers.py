from django.contrib.auth import get_user_model

from rest_framework import serializers

from .models import Persona


User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
        )

class PersonaSerializer(serializers.ModelSerializer):
    usuario = UserSerializer(read_only=True)

    class Meta:
        model = Persona
        fields = (
            'nombre',
            'apellido',
            'rol',
            'timestamp',
            'usuario'
        )
