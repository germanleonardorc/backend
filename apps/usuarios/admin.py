from django.contrib import admin

from apps.usuarios.models import Persona

# Register your models here.
admin.site.register(Persona)
