from django.db import models
from django.contrib.auth.models import User


class Persona(models.Model):
    ROLES = (
        ('0', 'Cliente'),
        ('1', 'Asesor'),
        ('2', 'Administrador')
    )

    nombre = models.CharField(max_length=70, null=False)
    apellido = models.CharField(max_length=70, null=False)
    rol = models.CharField(choices=ROLES, max_length=2)
    timestamp = models.DateTimeField(auto_now_add=True)

    usuario = models.OneToOneField(User,
                                   on_delete=models.CASCADE,
                                   null=False)

    def __str__(self):
        return self.nombre
