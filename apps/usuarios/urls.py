from django.conf.urls import url
from django.urls import path

from apps.usuarios import views as vista

urlpatterns = [
    path('', vista.Bienvenido),
    path('registro/admin/', vista.SuperRegistro.as_view()),
    path('registro/', vista.RegistroCliente.as_view()),
    path('login/', vista.Login.as_view()),
    path('perfil/', vista.PersonaDetail.as_view()),
    path('asesores/', vista.AsesorList.as_view()),
    path('asesor/<int:pk>/solicitudes/', vista.SolicitudAsesorList.as_view())
]
