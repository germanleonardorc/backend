from django.conf.urls import url
from django.urls import path

from apps.empresas import views as vista

urlpatterns = [
    path('todas/', vista.EmpresaList.as_view()),
    path('asesor/', vista.AsesorEmpresaList.as_view()),
    path('<int:pk>/asesores/', vista.AsesorEmpresaList.as_view()),
    path('<int:pk>/solicitudes/', vista.SolicitudEmpresaList.as_view())
]
