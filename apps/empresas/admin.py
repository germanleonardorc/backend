from django.contrib import admin

from apps.empresas.models import Empresa, AsesorEmpresa

# Register your models here.
admin.site.register(Empresa)
admin.site.register(AsesorEmpresa)
