from django.db import models

from apps.usuarios.models import Persona as asesor


class Empresa(models.Model):
    nombre = models.CharField(max_length=70, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre

class AsesorEmpresa(models.Model):
    asesor = models.ForeignKey(asesor, on_delete=models.SET_NULL, null=True)
    empresa = models.ForeignKey(Empresa, on_delete=models.SET_NULL, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
