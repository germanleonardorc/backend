from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from apps.solicitudes.models import Solicitud
from apps.usuarios.models import Persona
from .models import Empresa, AsesorEmpresa
from apps.solicitudes.serializers import SolicitudSerializer
from .serializers import EmpresaSerializer, AseosrEmpresaSerializer


class EmpresaList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Obtiene un listado de todas las empresas

        @return JSON
        """
        empresas = null = None
        if request.user.persona.rol == '2':
            empresas = Empresa.objects.all()
            serializer = EmpresaSerializer(empresas, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class AsesorEmpresaList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Registra un asesor para una empresa

        @return Estado 201 de creación
                Si se autentifica con un token no relacionado a un administrador
                obtendrá un código 401
                Si el usuario no tiene rol de asesor retorna un 404
                Retorna 409 si el asesor ya está asociado a esa empresa
        """
        if request.user.persona.rol == '2':
            asesor = request.data['asesor_id']
            empresa = request.data['empresa_id']

            if Persona.objects.get(id=asesor).rol == '1':
                if not AsesorEmpresa.objects.filter(asesor_id=asesor).filter(empresa_id=empresa).exists():
                    asesorempresa = AsesorEmpresa(asesor_id=asesor,empresa_id=empresa)
                    asesorempresa.save()

                    return Response(status=status.HTTP_201_CREATED)
                else:
                    return Response(status=status.HTTP_409_CONFLICT)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class AsesorEmpresaList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return AsesorEmpresa.objects.filter(empresa=pk)
        except AsesorEmpresa.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        Obtiene un listado de todos los asesores de una empresa

        @return JSON
                Si se autentifica con un token no relacionado a un administrador
                obtendrá un código 401
        """
        asesorempresa = null = None
        if request.user.persona.rol == '2':
            asesorempresa = self.get_object(pk)
            serializer = AseosrEmpresaSerializer(asesorempresa, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class SolicitudEmpresaList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Solicitud.objects.filter(empresa=pk)
        except Solicitud.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        Listado de todas las solicitudes asignadas a una empresa

        @return JSON
                Si el usuario no es administrador retorna un estado 401
                Retorna un estado 404 si no existe la empresa
        """
        if request.user.persona.rol == '2':
            solicitudes = self.get_object(pk)
            serializer = SolicitudSerializer(solicitudes, many=True)

            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
