from rest_framework import serializers

from .models import Empresa
from apps.usuarios.serializers import PersonaSerializer as AsesorSerializer

class EmpresaSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta():
        model = Empresa
        fields = (
            'id',
            'nombre',
            'timestamp'
        )

class AseosrEmpresaSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    asesor = AsesorSerializer(read_only=True)

    class Meta():
        model = Empresa
        fields = (
            'id',
            'asesor',
            'timestamp'
        )
